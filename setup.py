from setuptools import find_packages, setup

setup(
    name='dye',
    version='1.0.0',
    author='Aptivate Cooperators',
    author_email='tech@aptivate.org',
    packages=find_packages(exclude=['tests']),
    scripts=['dye/tasks.py'],
    url='git@git.coop:aptivate/dye.git',
    license='LICENSE.txt',
    description='Deploy Your Environment.',
    install_requires=[
        'fabric3',
        'docopt',
        'mysqlclient',
    ],
    test_requires=[
        'pytest'
    ]
)
