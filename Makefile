NETWORK   := dyenetwork
IMAGE     := dyebase
ROOTPW    := testroot
MYSQLNAME := mysql

docker_network_create:
	@docker network create $(NETWORK)

docker_network_delete:
	@docker network rm $(NETWORK)

docker_mysql:
	@docker run -d --name $(MYSQLNAME) --network $(NETWORK) -e MYSQL_ROOT_PASSWORD=$(ROOTPW) $(MYSQLNAME)

docker_dye_build:
	@docker build . -t $(IMAGE)

docker_dye: docker_dye_build
	@docker run -it -v $$(pwd):/var/dye --rm --network $(NETWORK) $(IMAGE) bash
