import os
import sqlite3

import pytest


@pytest.mark.parametrize(
    'sqlite_manager', ('sqlite', 'sqlite3'), indirect=True
)
def test_sqlite_dropping_always_works(sqlite_manager):
    try:
        sqlite_manager.drop_db()
    except Exception:
        pytest.fail('Dropping non-existent table raised errors!')


@pytest.mark.parametrize(
    'sqlite_manager', ('sqlite', 'sqlite3'), indirect=True
)
def test_sqlite_missing_table(sqlite_manager):
    assert not sqlite_manager.test_db_table_exists('testtable')


@pytest.mark.parametrize(
    'sqlite_manager', ('sqlite', 'sqlite3'), indirect=True
)
def test_sqlite_db_and_table_creation(sqlite_manager):
    try:
        connection = sqlite3.connect(sqlite_manager.file_path)
        connection.execute('CREATE TABLE testtable (mycolumn CHAR(30))')
        connection.close()
    except Exception:
        pytest.fail('Failed to create sqlite database!')

    assert os.path.exists(sqlite_manager.file_path)
    assert sqlite_manager.test_db_table_exists('testtable')
