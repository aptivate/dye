import pytest


@pytest.mark.parametrize('host_set, host_get', (
    (None, 'localhost'),
    ('', 'localhost'),
    ('12.34.56.78', '12.34.56.78'),
))
def test_mysql_init(host_set, host_get, mysql_defaults, create_mysql_manager):
    mysql_defaults['host'] = host_set
    manager = create_mysql_manager(**mysql_defaults)
    assert manager.host == host_get


def test_mysql_cmd_args(mysql_manager):
    sql_args = mysql_manager.create_cmdline_args()
    expected_args = [
        '-u', mysql_manager.user,
        '-p{}'.format(mysql_manager.password),
        '--host={}'.format(mysql_manager.host),
        '--port={}'.format(mysql_manager.port),
        mysql_manager.name
    ]
    assert expected_args == sql_args


def test_mysql_test_user_doesnt_exist(mysql_manager):
    assert not mysql_manager.test_sql_user_exists()
    assert not mysql_manager.test_sql_user_password()


def test_mysql_test_user_exists(mysql_manager, mysql_user):
    assert mysql_manager.test_sql_user_exists()
    assert mysql_manager.test_sql_user_password()


def test_mysql_passwords(mysql_manager, mysql_user):
    assert not mysql_manager.test_sql_user_password(password='WRONG')
    assert not mysql_manager.test_root_password('WRONG')
    assert mysql_manager.test_root_password(mysql_manager.root_password)


def test_mysql_grants(mysql_manager, mysql_db, mysql_user, mysql_grants):
    assert mysql_manager.test_grants()


def test_mysql_db_doesnt_exist(mysql_manager):
    assert not mysql_manager.db_exists()


def test_mysql_db_exists(mysql_manager, mysql_db):
    assert mysql_manager.db_exists()


def test_mysql_table_exists(mysql_manager, mysql_user,
                            mysql_grants, mysql_db, mysql_table):
    assert mysql_manager.test_db_table_exists('testtable')


def test_mysql_table_doesnt_exist(mysql_manager, mysql_user,
                                  mysql_grants, mysql_db):
    assert not mysql_manager.test_db_table_exists('testtable')


def test_mysql_create_db_if_not_exists(mysql_manager):
    assert not mysql_manager.db_exists()
    mysql_manager.create_db_if_not_exists()
    assert mysql_manager.db_exists()
