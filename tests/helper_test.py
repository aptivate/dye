from dye.helper import set_dict_if_not_set


def test_item_added_if_not_present():
    testdict = {'a': 'b'}
    set_dict_if_not_set(testdict, 'b', 'c')
    assert testdict['b'] == 'c'


def test_item_not_added_if_present():
    testdict = {'a': 'b'}
    set_dict_if_not_set(testdict, 'a', 'c')
    assert testdict['a'] == 'b'
