import pytest

import os

from dye.tasklib import tasklib
from dye.tasklib.exceptions import InvalidProjectError


def test_raises_when_missing_settings(local_settings_py_dev):
    with pytest.raises(InvalidProjectError):
        tasklib.link_local_settings('dev')


def test_raises_settings_no_local_import(empty_settings_py,
                                         local_settings_py_dev):
        with pytest.raises(InvalidProjectError):
            tasklib.link_local_settings('dev')


def test_raises_settings_without_local_link(settings_py):
    with pytest.raises(InvalidProjectError):
        tasklib.link_local_settings('dev')


def test_local_link(settings_py, local_settings_py_dev):
    tasklib.link_local_settings('dev')

    django_dir = tasklib.env['django_settings_dir']
    local_settings = os.path.join(django_dir, 'local_settings.py')
    assert os.path.islink(local_settings)

    link_to = os.readlink(local_settings)
    assert os.path.basename(link_to) == 'local_settings.py.dev'


def test_local_link_overwrites(settings_py, local_settings_py_dev):
    django_dir = tasklib.env['django_settings_dir']
    local_settings = os.path.join(django_dir, 'local_settings.py')

    empty_handle = open(local_settings, 'a')
    empty_handle.close()

    assert not os.path.islink(local_settings)

    tasklib.link_local_settings('dev')

    assert os.path.islink(local_settings)

    link_to = os.readlink(local_settings)
    assert os.path.basename(link_to) == 'local_settings.py.dev'
