from dye import tasks


def test_main_h_exits_with_0():
    exit_code = tasks.main(['-h'])
    assert 0 == exit_code


def test_convert_argument_converts_true_to_boolean_true():
    result = tasks.convert_argument('true')
    assert result


def test_convert_argument_converts_false_to_boolean_false():
    result = tasks.convert_argument('false')
    assert not result


def test_convert_argument_converts_numeric_test_to_number():
    result = tasks.convert_argument('15')
    assert result == 15


def test_convert_argument_leaves_other_text_as_text():
    result = tasks.convert_argument('sometext')
    assert result == 'sometext'


def test_convert_task_bits_deals_with_lone_function():
    func, pos_args, kwargs = tasks.convert_task_bits('func')
    assert func == 'func'
    assert pos_args == []
    assert kwargs == {}


def test_convert_task_bits_deals_with_function_and_one_pos_arg():
    func, pos_args, kwargs = tasks.convert_task_bits('func:arg1')
    assert func == 'func'
    assert pos_args == ['arg1']
    assert kwargs == {}


def test_convert_task_bits_deals_with_function_and_multiple_pos_args():
    func, pos_args, kwargs = tasks.convert_task_bits('func:arg1,arg2')
    assert func == 'func'
    assert pos_args == ['arg1', 'arg2']
    assert kwargs == {}


def test_convert_task_bits_deals_with_function_and_one_kw_arg():
    func, pos_args, kwargs = tasks.convert_task_bits('func:kw1=arg1')
    assert func == 'func'
    assert pos_args == []
    assert kwargs == {'kw1': 'arg1'}


def test_convert_task_bits_deals_with_function_and_multiple_kw_args():
    func, pos_args, kwargs = tasks.convert_task_bits('func:kw1=arg1,kw2=arg2')
    assert func == 'func'
    assert pos_args == []
    assert kwargs == {'kw1': 'arg1', 'kw2': 'arg2'}


def test_convert_task_bits_deals_with_both_pos_args_and_kw_args():
    func, pos_args, kwargs = tasks.convert_task_bits('func:arg,kw=arg')
    assert func == 'func'
    assert pos_args == ['arg']
    assert kwargs == {'kw': 'arg'}


def test_get_public_callables_returns_public_functions(testmod):
    def public_function():
        pass

    testmod.public_function = public_function
    public_callables = tasks.get_public_callables(testmod)

    assert public_callables == ['public_function']


def test_get_public_callables_does_not_return_private_functions(testmod):
    def private_function():
        pass

    testmod._private_function = private_function
    public_callables = tasks.get_public_callables(testmod)

    assert '_private_function' not in public_callables


def test_get_public_callables_does_not_return_classes(testmod):
    class a_class(object):
        def a_method():
            pass

    testmod.a_class = a_class
    public_callables = tasks.get_public_callables(testmod)

    assert 'a_class' not in public_callables


def test_get_public_callables_does_not_return_variables(testmod):
    testmod.a_string = 'a string'
    public_callables = tasks.get_public_callables(testmod)

    assert 'a string' not in public_callables


def test_get_public_callables_returns_empty_list_when_passed_none():
    public_callables = tasks.get_public_callables(None)
    assert public_callables == []
