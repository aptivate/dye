"""Fixtures for pytest."""

import functools
import os
import shutil
import sys

import pytest


@pytest.fixture
def testmod():
    from .testdeploy import empty_module as test_module

    yield test_module


@pytest.fixture
def testdir():
    return os.path.join(os.path.dirname(__file__), 'testdir')


@pytest.fixture
def project_settings(testdir):
    current_working_directory = os.path.dirname(__file__)
    path = os.path.join(
        current_working_directory, os.pardir,
        '{{cookiecutter.project_name}}', 'deploy'
    )
    sys.path.append(path)

    import project_settings

    project_settings.project_name = 'testproj'
    project_settings.django_apps = ['testapp']
    project_settings.project_type = 'django'
    project_settings.use_virtualenv = False
    project_settings.local_deploy_dir = os.path.dirname(__file__)
    project_settings.local_vcs_root = testdir

    project_settings.relative_django_dir = os.path.join(
        'django',
        project_settings.project_name
    )

    project_settings.django_dir = os.path.join(
        project_settings.local_vcs_root,
        project_settings.relative_django_dir
    )

    project_settings.relative_django_settings_dir = os.path.join(
        project_settings.relative_django_dir,
        project_settings.project_name
    )

    project_settings.relative_ve_dir = os.path.join(
        project_settings.relative_django_dir,
        '.ve'
    )

    return project_settings


@pytest.fixture
def tasklib_env(project_settings, testdir):
    from dye.tasklib import tasklib

    tasklib.env['verbose'] = False
    tasklib.env['quiet'] = True
    tasklib.env['noinput'] = True

    tasklib._setup_paths(project_settings, None)

    django_dir = tasklib.env['django_dir']
    ve_dir = tasklib.env['ve_dir']

    tasklib.env['python_bin'] = os.path.join(ve_dir, 'bin', 'python')
    tasklib.env['manage_py'] = os.path.join(django_dir, 'manage.py')

    if not os.path.exists(tasklib.env['django_dir']):
        os.makedirs(tasklib.env['django_dir'])
        os.makedirs(tasklib.env['django_settings_dir'])

    yield tasklib.env

    shutil.rmtree(testdir)


@pytest.fixture
def local_settings_py_dev(tasklib_env):
    settings = tasklib_env['django_settings_dir']
    settings_path = os.path.join(settings, 'local_settings.py.dev')
    with open(settings_path, 'w') as settings:
        settings.write('# python file')
    yield settings_path
    os.remove(settings_path)


@pytest.fixture
def empty_settings_py(tasklib_env):
    django_dir = tasklib_env['django_settings_dir']
    settings_path = os.path.join(django_dir, 'settings.py')
    empty_handle = open(settings_path, 'a')
    empty_handle.close()
    yield empty_handle
    os.remove(settings_path)


@pytest.fixture
def settings_py(tasklib_env):
    django_dir = tasklib_env['django_settings_dir']
    settings_path = os.path.join(django_dir, 'settings.py')
    with open(settings_path, 'w') as handle:
        handle.write('import local_settings')
    yield handle
    os.remove(settings_path)


@pytest.fixture
def sqlite_manager(request):
    from dye.tasklib import database

    manager = database.get_db_manager(
        engine=request.param,
        name='testdb',
        root_dir='.'
    )

    yield manager

    manager.drop_db()


@pytest.fixture
def mysql_defaults():
    return dict(
        engine='mysql',
        name='testdb',
        user='testuser',
        password='testpassword',
        port=os.environ['TEST_DB_PORT'],
        host=os.environ['TEST_DB_HOST'],
        root_password=os.environ['TEST_ROOT_PASSWORD'],
        grant_enabled=True,
    )


@pytest.fixture
def mysql_manager(mysql_defaults):
    from dye.tasklib import database

    yield database.get_db_manager(**mysql_defaults)


@pytest.fixture
def create_mysql_manager():
    from dye.tasklib import database

    yield functools.partial(database.get_db_manager)


@pytest.fixture
def mysql_user(mysql_manager):
    mysql_manager.create_user_if_not_exists()
    yield mysql_manager
    mysql_manager.drop_user()


@pytest.fixture
def mysql_db(mysql_manager):
    mysql_manager.create_db_if_not_exists()
    yield mysql_manager
    mysql_manager.drop_db()


@pytest.fixture
def mysql_grants(mysql_manager):
    mysql_manager.grant_all_privileges_for_database()


@pytest.fixture
def mysql_table(mysql_manager):
    statement = 'CREATE TABLE {}.{}(mycolumn CHAR(30))'
    mysql_manager.exec_as_root(
        statement.format(mysql_manager.name, 'testtable')
    )

    yield mysql_manager

    statement = 'DROP TABLE {}.{}'
    mysql_manager.exec_as_root(
        statement.format(mysql_manager.name, 'testtable')
    )
